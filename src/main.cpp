//============================================================================
// Name        : main.cpp
// Author      : Arthur Covanov
// Version     :
// Copyright   : GPL
// Description : Simpliest GLEW/GLUT application example
//============================================================================
#include <stdio.h>
#include <stdlib.h>
//#include <GL/gl.h> //NOT WITH GLEW
#include "GL/glew.h"
#include "GL/freeglut.h"

#define BUFFER_OFFSET(bytes) ((GLubyte*) NULL + (bytes))

GLuint vao;
GLuint shader_programme;

void initShaders()
{
	shader_programme = glCreateProgram();

	printf("Shader_Program: %i\n", shader_programme);
	const char* vertex_shader =
	"#version 400\n"
	"in vec3 vp;"
	"void main()"
	"{"
	"  gl_Position = vec4 (vp, 1.0);"
	"}";

	const char* fragment_shader =
	"#version 400\n"
	"out vec4 frag_colour;"
	"void main()"
	"{"
	"  frag_colour = vec4 (1.0, 1.0, 1.0, 1.0);"
	"}";

	GLuint vs = glCreateShader (GL_VERTEX_SHADER);
	glShaderSource (vs, 1, &vertex_shader, NULL);
	glCompileShader (vs);
	GLuint fs = glCreateShader (GL_FRAGMENT_SHADER);
	glShaderSource (fs, 1, &fragment_shader, NULL);
	glCompileShader (fs);

	glAttachShader(shader_programme, fs);
	glAttachShader(shader_programme, vs);
	glLinkProgram(shader_programme);
	glUseProgram(shader_programme);
}

void init()
{
	printf("init\n");

	float points[] = {
	   0.0f,  0.5f,  0.0f,
	   0.5f, -0.5f,  0.0f,
	  -0.5f, -0.5f,  0.0f
	};

	GLuint vbo = 0;
	glGenBuffers (1, &vbo);
	glBindBuffer (GL_ARRAY_BUFFER, vbo);
	glBufferData (GL_ARRAY_BUFFER, 9 * sizeof (float), points, GL_STATIC_DRAW);

	glGenVertexArrays(1, &vao);
	glBindVertexArray (vao);
	glEnableVertexAttribArray (0);
	glBindBuffer (GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	initShaders();
}

void display(void)
{
	//printf("display\n");
	glClear (GL_COLOR_BUFFER_BIT);
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glFlush();
}

int main(int argc, char *argv[])
{
	printf("Simplest OpenGL 4.5 example\n");
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);
	glutInitWindowSize(512, 512);
	glutInitContextVersion(4, 5);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutCreateWindow(argv[0]);
	printf("GL: %s\n", glGetString(GL_VERSION));

	glewExperimental = GL_TRUE;
	if(glewInit())
	{
		printf("Unable to init GLEW!!!\n");
		return EXIT_FAILURE;
	}

	init();

	glutDisplayFunc(display);

	glutMainLoop();

	return EXIT_SUCCESS;
}
