CXXFLAGS =	-O2 -g -Wall -fmessage-length=0

SDIR	= src
IDIR	= inc
LDIR	= lib
ODIR	= obj

OBJS	= main.o

TARGET	= bin/main.exe

main.o: $(SDIR)/main.cpp
	$(CXX) -o $(ODIR)/$@ -c $^ -I $(IDIR)

$(TARGET):	$(OBJS)
	$(CXX) -o $@ $(ODIR)/* -L $(LDIR) -lopengl32 -lfreeglut -lglew32

all:	clean $(TARGET)

clean:
	rm -f $(ODIR)/* $(TARGET)
